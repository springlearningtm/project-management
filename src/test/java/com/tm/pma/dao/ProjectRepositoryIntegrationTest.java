package com.tm.pma.dao;

import com.tm.pma.entities.Project;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
@Sql("classpath:test-data.sql")
public class ProjectRepositoryIntegrationTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    public void ifNewProjectSaveThenSuccess() {
        Project project = new Project("New Test Project", "COMPLETE", "Test description");
        projectRepository.save(project);
        assertEquals(5, projectRepository.findAll().size());
    }
}
