package com.tm.pma.dao;

import com.tm.pma.dto.ProjectStageCount;
import com.tm.pma.entities.Project;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProjectRepository extends CrudRepository<Project, Long> {

    @Override
    public List<Project> findAll();

    @Query(nativeQuery = true, value = "select stage as label, COUNT(*) as value FROM project GROUP BY stage")
    public List<ProjectStageCount> projectStageCount();
}
