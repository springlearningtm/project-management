package com.tm.pma.dao;

import com.tm.pma.dto.EmployeeProject;
import com.tm.pma.entities.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    @Override
    public List<Employee> findAll();

    @Query(nativeQuery = true, value = "select e.first_name as firstName, e.last_name as lastName, e.email as email,  COUNT(pe.employee_id) as projectCount " +
            "FROM employee e left join project_employee pe ON pe.employee_id = e.employee_id " +
            "GROUP BY e.first_name, e.last_name, e.email ORDER BY 3 DESC")
    public List<EmployeeProject> employeeProjects();
}
