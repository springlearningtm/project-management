package com.tm.pma.dto;

public interface ProjectStageCount {
    public String getLabel();
    public int getValue();
}
