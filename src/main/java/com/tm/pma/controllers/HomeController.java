package com.tm.pma.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tm.pma.dao.EmployeeRepository;
import com.tm.pma.dao.ProjectRepository;
import com.tm.pma.dto.ProjectStageCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    @Value("${version}")
    private String version;

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/")
    public String displayHome(Model model) throws JsonProcessingException {
        model.addAttribute("projects", projectRepository.findAll());
        model.addAttribute("versionNumber", version);

        List<ProjectStageCount> projectStageCounts = projectRepository.projectStageCount();
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(projectStageCounts);
        model.addAttribute("projectStagesCount", jsonString);

        model.addAttribute("employeeProjects", employeeRepository.employeeProjects());

        return "main/home";
    }
}
