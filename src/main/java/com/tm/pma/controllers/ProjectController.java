package com.tm.pma.controllers;

import com.tm.pma.dao.EmployeeRepository;
import com.tm.pma.dao.ProjectRepository;
import com.tm.pma.entities.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/new")
    public String displayProjectForm(Model model) {
        model.addAttribute("project", new Project());
        model.addAttribute("allEmployees", employeeRepository.findAll());
        return "projects/new-project";
    }

    @PostMapping( "/save")
    public String createProject(Project project, @RequestParam List<Long> employees) {
        projectRepository.save(project);
        //use a redirect to prevent duplicate submissions
        return "redirect:/projects";
    }

    @GetMapping
    public String displayProjects(Model model) {
        model.addAttribute("projects", projectRepository.findAll());

        return "projects/list-projects";
    }
}
