package com.tm.pma.controllers;

import com.tm.pma.dao.EmployeeRepository;
import com.tm.pma.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/new")
    public String displayEmployeeForm(Model model) {
        model.addAttribute("employee", new Employee());
        return "employees/new-employee";
    }

    @PostMapping("/save")
    public String createEmployee(Employee employee) {
        employeeRepository.save(employee);
        //use a redirect to prevent duplicate submissions
        return "redirect:/employees/new";
    }

    @GetMapping
    public String displayEmployees(Model model) {
        model.addAttribute("employeeProjects", employeeRepository.employeeProjects());
        return "employees/list-employees";
    }
}
