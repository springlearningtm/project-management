function decodeHtml(html) {
    const txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

const charDataStr = decodeHtml(chartData);
const charJsonArray = JSON.parse(charDataStr);

const arrayLength = charJsonArray.length;
const numericData = [];
const labelData = [];

for (let i = 0; i < arrayLength; i++) {
    numericData[i] = charJsonArray[i].value;
    labelData[i] = charJsonArray[i].label;
}

const data = {
    labels: labelData,
    datasets: [{
        label: 'Project Statuses',
        data: numericData,
        backgroundColor: [
            'rgb(255, 99, 132)',
            'rgb(54, 162, 235)',
            'rgb(255, 205, 86)'
        ],
        hoverOffset: 4
    }]
};
const config = {
    type: 'pie',
    data: data,
    options: {
        plugins: {
            title: {
                display: true,
                text: "Project Statuses"
            }
        }
    }
};

new Chart(
    document.getElementById('pieChart'),
    config
);