#Docker commands

##Containers
###Run the container on port 8080
docker container run -p 8080:8080 pma-docker-image

###Run the container on port 8080 in detached mode
docker container run -d -p 8080:8080 pma-docker-image

###Stop the container 
docker stop #container-id

###List running containers
docker container ps
####OR
docker container ls

###Show container ran in the past
docker container ls -a

###Show container logs
docker container logs #container-id

###Start the old container
docker start #container-id

###Delete all containers that are not running
docker container prune

##Images
###List images
docker images

###Delete all images
docker image prune -a

###Build an image
docker build -t #name .

###Delete image
docker rmi image #image